from facadedevice import Facade, combined_attribute, state_attribute
from facadedevice import logical_attribute
from tango import CmdArgType, DevState
from tango.server import device_property


class StateComposer(Facade):
    state_prios = device_property(
        dtype=(str,),
        default_value=[
            "ON", "RUNNING", "OPEN", "EXTRACT",
            "STANDBY", "OFF", "CLOSE", "INSERT", "MOVING",
            "DISABLE", "INIT", "ALARM", "FAULT", "UNKNOWN"])

    _state_to_number = None
    _number_to_state = None

    def state_to_number(self, state):
        if not self._state_to_number:
            dic = {b: a for a, b in enumerate(self.state_prios)}
            self._state_to_number = dic
        return self._state_to_number[state]

    def number_to_state(self, number):
        if not self._number_to_state:
            dic = {a: b for a, b in enumerate(self.state_prios)}
            self._number_to_state = dic
        return self._number_to_state[number]

    @combined_attribute(
        dtype=(CmdArgType.DevState,),
        max_dim_x=1024,
        property_name="States")
    def row_states(self, *states):
        return states

    @logical_attribute(
        dtype=(str,),
        bind=["row_states"],
        max_dim_x=1024)
    def devices_states(self, row_states):
        subs = self.graph.subnodes("row_states")
        res = []
        for sub in subs:
            device_name = "/".join(sub.remote_attr.split("/")[:-1])
            value = sub.result().value
            res.append("{}: {}".format(device_name, value))
        return res

    @state_attribute(bind=["row_states"])
    def state_and_status(self, row_states):
        states = [self.state_to_number(str(state)) for state in row_states]
        state_n = max(states)
        state_str = self.number_to_state(state_n)
        status = "Combined state is {}".format(state_str)
        return getattr(DevState, state_str), status


def run():
    StateComposer.run_server()


if __name__ == "__main__":
    run()
