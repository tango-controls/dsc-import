#!/usr/bin/env python
# -*- coding:utf-8 -*-

""" Tango device server for SISOPID control loop """
import traceback
from functools import wraps
from facadedevice import TimedFacade
from tango import (
    AttrWriteType,
    AttributeProxy,
    DevState,
    AttrQuality,
    DispLevel,
    DevFailed,
)
from tango.server import command, device_property
from tango.server import attribute
from simple_pid import PID
from time import time


def handle_exception(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except Exception as e:
            self.set_state(DevState.ALARM)
            self.set_status(
                str(func.__name__) + "\nException caught! Resolve issue and run"
                                     "Start again. \n\n" + str(e)
            )
            raise

    return wrapper


class SISOPID(TimedFacade):
    """ SISOPID tango TimedFacade device class. """

    Sensor = device_property(dtype=str, default_value="", doc="Tango Attribute sensor")

    Actuator = device_property(
        dtype=str, default_value="", doc="Tango attribute process variable"
    )

    ActuatorEnvelope = device_property(
        dtype=(float,), default_value=[], doc="Min and Max limits for actuator"
    )

    InterlockList = device_property(
        dtype=(str,), default_value=[], doc="Interlock attribute list"
    )

    InterlockCondition = device_property(
        dtype=(str,), default_value=[], doc="Interlock condition list"
    )

    # Local attributes

    Kp = attribute(
        dtype=float,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ_WRITE,
        label="Kp",
        format="4.4f",
        fget="read_Kp",
        fset="write_Kp",
        memorized=True,
        hw_memorized=True,
        doc="Proportional controller gain",
    )

    def read_Kp(self):
        try:
            return self._Kp
        except Exception:
            self._Kp = 1.0
            return self._Kp

    @handle_exception
    def write_Kp(self, value):
        self._Kp = value
        if self.get_state() == DevState.RUNNING:
            self.pid.Kp = value

    Ki = attribute(
        dtype=float,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ_WRITE,
        label="Ki",
        format="4.4f",
        fget="read_Ki",
        fset="write_Ki",
        memorized=True,
        hw_memorized=True,
        doc="Integral controller gain",
    )

    def read_Ki(self):
        try:
            return self._Ki
        except Exception:
            self._Ki = 0.0
            return self._Ki

    @handle_exception
    def write_Ki(self, value):
        self._Ki = value
        if self.get_state() == DevState.RUNNING:
            self.pid.Ki = value

    Kd = attribute(
        dtype=float,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ_WRITE,
        label="Kd",
        format="4.4f",
        fget="read_Kd",
        fset="write_Kd",
        memorized=True,
        hw_memorized=True,
        doc="Derivative controller gain",
    )

    def read_Kd(self):
        try:
            return self._Kd
        except Exception:
            self._Kd = 0.0
            return self._Kd

    @handle_exception
    def write_Kd(self, value):
        self._Kd = value
        if self.get_state() == DevState.RUNNING:
            self.pid.Kd = value

    setpoint = attribute(
        dtype=float,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ_WRITE,
        label="setpoint",
        format="4.4f",
        fget="read_setpoint",
        fset="write_setpoint",
        memorized=True,
        hw_memorized=True,
        doc="Derivative controller gain",
    )

    def read_setpoint(self):
        try:
            return self._setpoint
        except Exception:
            self._setpoint = 0.0
            return self._setpoint

    @handle_exception
    def write_setpoint(self, value):
        self._setpoint = value
        if self.get_state() == DevState.RUNNING:
            self.pid.setpoint = value

    correction = attribute(
        dtype=float,
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
        label="correction",
        doc="Derivative controller gain",
    )

    def read_correction(self):
        if self.get_state() != DevState.RUNNING:
            return 0.0, time(), AttrQuality.ATTR_INVALID
        else:
            try:
                return self._correction
            except Exception:
                return 0.0, time(), AttrQuality.ATTR_INVALID

    def init_device(self):
        """
        init device
        """
        self._running = False
        TimedFacade.init_device(self)

    def safe_init_device(self):
        """
        safe init device
        """
        TimedFacade.safe_init_device(self)
        try:
            self.setup_proxies()
            self.set_state(DevState.STANDBY)
            self.set_status("SISOPID in standby\nCorrection is not being applied.")
        except (ValueError, TypeError) as e:
            self.set_state(DevState.FAULT)
            status = (
                "The device is in FAULT because the raised error: {}.\n"
                "Fix the problem and execute the command Init()\n{}"
            )
            status = status.format(e, traceback.format_exc())
            self.set_status(status)
        except DevFailed as e:
            self.set_state(DevState.FAULT)
            status = (
                "The device is in FAULT because it failed to open"
                "a DeviceProxy.\nThe error is {}.\n"
                "Fix the problem and execute the command Init()\n{}"
            )
            status = status.format(e, traceback.format_exc())
            self.set_status(status)
        except Exception as e:
            self.set_state(DevState.FAULT)
            status = (
                "The device is in FAULT because: {}.\n"
                "Fix the problem and execute the command Init()\n{}"
            )
            status = status.format(e, traceback.format_exc())
            self.set_status(status)

    def setup_proxies(self):
        """
        create tango attribute proxies
        """
        self._sensor = AttributeProxy(self.Sensor)
        self._actuator = AttributeProxy(self.Actuator)
        self._interlocks = list(map(AttributeProxy, self.InterlockList))

    def setup_pid(self):
        """
        create interpolation function
        """
        self.pid = PID(Kp=self._Kp, Ki=self._Ki, Kd=self._Kd, setpoint=self._setpoint)

    @property
    def is_running(self):
        return self._running

    @handle_exception
    def on_time(self, t):
        """
        Control loop action
        """
        if self._running:
            interlocks = [str(proxy.read().value) for proxy in self._interlocks]
            status = ""
            for i, data in enumerate(zip(interlocks, self.InterlockCondition)):
                interlock, interlock_state = data
                if interlock == interlock_state:
                    status += "Interlock: {} is active in {}\n".format(
                        self.InterlockList[i], interlock_state
                    )
            if status != "":
                self.set_state(DevState.ALARM)
                self.set_status(status)
            else:
                sensor_quality = self._sensor.read().quality
                if sensor_quality != AttrQuality.ATTR_VALID:
                    self.set_state(DevState.ALARM)
                    self.set_status(
                        "Sensor attribute quality is: {}".format(sensor_quality)
                    )
                else:
                    self.set_state(DevState.RUNNING)
                    sensor_data = self._sensor.read().value
                    actuator_data = self._actuator.read().value
                    self._correction = self.pid(sensor_data)
                    actuator_setpoint = actuator_data + self._correction
                    try:
                        if len(self.ActuatorEnvelope) == 2:
                            if (
                                    actuator_setpoint >= self.ActuatorEnvelope[0]
                                    and actuator_setpoint <= self.ActuatorEnvelope[1]
                            ):
                                self._actuator.write(actuator_setpoint)
                                self.set_status(
                                    "SISOPID running.\nCorrection is being applied"
                                )
                            else:
                                self.set_status(
                                    "SISOPID running.\n"
                                    "Correction not applied.\n"
                                    "Out of actuator envelope, correction is: {}".format(
                                        actuator_setpoint
                                    )
                                )
                        else:
                            self._actuator.write(actuator_setpoint)
                            self.set_status(
                                "SISOPID running.\nCorrection is being applied"
                            )
                    except DevFailed as e:
                        self.set_status(
                            "SISOPID running.\n"
                            "Correction not applied.\n"
                            "Error: {}".format(e)
                        )

    @command
    def Start(self):
        """ Start control loop. """
        self.setup_pid()
        self._running = True
        self.set_state(DevState.RUNNING)
        self.set_status("SISOPID running.\nCorrection is being applied")

    def is_Start_allowed(self):
        return not self._running and self.get_state() not in [
            DevState.FAULT,
            DevState.RUNNING,
        ]

    @command
    def Stop(self):
        """ Stop control loop. """
        self._running = False
        self.set_state(DevState.STANDBY)
        self.set_status("SISOPID in standby\nCorrection is not being applied.")

    def is_Stop_allowed(self):
        return self._running and self.get_state() not in [
            DevState.FAULT,
            DevState.STANDBY,
        ]


def run():
    SISOPID.run_server()


if __name__ == "__main__":
    run()
