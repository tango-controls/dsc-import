import pytest
import sys
import python_hl_utils
from lxml import etree
import os

ASSETS_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), "assets")


def test_tango_device_class():
    py_file1 = f'{ASSETS_PATH}/RaspberryPiIO.py'

    xmi = python_hl_utils.get_xmi_from_python_hl(
        'RaspberryPiIO', 'Communication', 'file:' + py_file1,
        meta_data={'author': 'pg@pg.com'}
    )
    print(xmi)
    assert xmi is not None
    xmi_parsed = etree.fromstring(xmi.encode('ascii'))
    class_name = (xmi_parsed.find('./classes').attrib['name'])
    assert class_name == "RaspberryPiIO"


def test_tango_timedfacade_class():
    py_file1 = f'{ASSETS_PATH}/sisopid.py'

    xmi = python_hl_utils.get_xmi_from_python_hl(
        'SISOPID', 'Communication', 'file:' + py_file1,
        meta_data={'author': 'pg@pg.com'}
    )
    print(xmi)
    assert xmi is not None
    xmi_parsed = etree.fromstring(xmi.encode('ascii'))
    class_name = (xmi_parsed.find('./classes').attrib['name'])
    assert class_name == "SISOPID"


def test_tango_facade_class():
    py_file1 = f'{ASSETS_PATH}/statecomposer.py'

    xmi = python_hl_utils.get_xmi_from_python_hl(
        'StateComposer', 'Communication', 'file:' + py_file1,
        meta_data={'author': 'pg@pg.com'}
    )
    print(xmi)
    assert xmi is not None
    xmi_parsed = etree.fromstring(xmi.encode('ascii'))
    class_name = (xmi_parsed.find('./classes').attrib['name'])
    assert class_name == "StateComposer"