FROM ghcr.io/astral-sh/uv:python3.11-bookworm

RUN apt-get update \
  && apt-get install -y \
    subversion \
    git \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /
RUN python3 -m venv /env \
  && /env/bin/python -m pip install -r /requirements.txt

WORKDIR /app
COPY . /app

ENV PATH /env/bin:$PATH
